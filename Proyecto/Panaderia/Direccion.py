import tkinter as tk
from tkinter import ttk
from Cliente import cliente
from Poblacion import poblacion


class direccion:

    def __init__(self,root, db):

        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("700x400")
        self.root.title("Dirección")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__config_treeview_direccion()
        self.__config_buttons_direccion()