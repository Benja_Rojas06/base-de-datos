import tkinter as tk
from tkinter import ttk
from Sector import sector


class poblacion:

    def __init__(self,root,db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("550x400")
        self.root.title("Población")
        self.root.resizable(width=0, height=0)

        self.root.transient(root)

        self.__config_treeview_poblacion()
        self.__config_buttons_poblacion()


    def __config_treeview_poblacion(self):
        self.treeview =  ttk.Treeview(self.root)
        self.treeview.configure(columns=("#1","#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Población")
        self.treeview.heading("#2", text = "Sector")

        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.column("#2", minwidth = 147, width = 147, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 550)
        self.llenar_treeview_poblacion()
        self.root.after(0, self.llenar_treeview_poblacion)

    def __config_buttons_poblacion(self):
        tk.Button(self.root, text="Insertar Población",
            command = self.__insertar_poblacion).place(x = 0, y = 350, width = 150, height = 50)
        tk.Button(self.root, text="Modificar Población",
            command = self.__modificar_poblacion).place(x = 200, y = 350, width = 150, height = 50)
        tk.Button(self.root, text="Eliminar Población",
           command = self.__eliminar_poblacion).place(x = 400, y = 350, width = 150, height = 50)


    def llenar_treeview_poblacion(self):
        sql = """select poblacion.id_poblacion, poblacion.nom_poblacion, sector.nom_sector
        from poblacion inner join sector on poblacion.sector_id_sector = sector.id_sector;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]+ " " +i[2]), iid = i[0])
            self.data = data

    def __insertar_poblacion(self):
        insertar_poblacion(self.db,self)

    def __eliminar_poblacion(self):
        sql = "delete from poblacion where id_poblacion = %(id_poblacion)s"
        self.db.run_sql_consultas(sql, {"id_poblacion": self.treeview.focus()})
        self.llenar_treeview_poblacion()

    def __modificar_poblacion(self):
        if(self.treeview.focus() != ""):
            sql = """select poblacion.id_poblacion, poblacion.nom_poblacion, sector.nom_sector
            from poblacion inner join sector on poblacion.sector_id_sector = sector.id_sector
            where id_poblacion = %(id_poblacion)s;"""
            row_data = self.db.run_select_filter(sql, {"id_poblacion": self.treeview.focus()})[0]
            modificar_poblacion(self.db, self, row_data)



class insertar_poblacion:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Población")
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label(self.insert_datos, text = "Población: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Sector: ").place(x = 10, y = 70, width = 80, height = 20)

    def __config_entry(self):
        self.entry_poblacion = tk.Entry(self.insert_datos)
        self.entry_poblacion.place(x = 110, y = 10, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.__fill_combo()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)


    def __fill_combo(self):
        sql = "select id_sector, nom_sector from sector"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into poblacion (sector_id_sector, nom_poblacion)
            values (%(sector_id_sector)s, %(nom_poblacion)s)"""
        self.db.run_sql_consultas(sql, {"sector_id_sector": self.ids[self.combo.current()],
            "nom_poblacion": self.entry_poblacion.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_poblacion()

class modificar_poblacion:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar Población")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Población ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Sector: ").place(x = 10, y = 40, width = 80, height = 20)


    def config_entry(self):#Se configuran los inputs
        self.entry_poblacion = tk.Entry(self.insert_datos)
        self.entry_poblacion.place(x = 110, y = 10, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.fill_combo()
        self.entry_poblacion.insert(0, self.row_data[1])
        self.combo.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update poblacion set nom_poblacion = %(nom_poblacion)s,
            sector_id_sector = %(sector_id_sector)s where id_poblacion = %(id_poblacion)s"""
        self.db.run_sql_consultas(sql, {"nom_poblacion": self.entry_poblacion.get(),
                        "sector_id_sector": self.ids[self.combo.current()],
                        "id_poblacion": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_poblacion()

    def fill_combo(self): #
        sql = "select id_sector, nom_sector from sector"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]