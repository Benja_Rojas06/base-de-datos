import mysql.connector

class Base_de_datos:

    def __init__(self):
        self.db = None
        self.cursor = None

        try:
            self.db = mysql.connector.connect(host = "localhost", user = "panaderia",
                                              passwd = "tiacarmela", database = "db_tiacarmela")
            self.cursor = self.db.cursor()
            print(" Usted se ha conectado de forma exitosa a la base de datos ")

        except mysql.connector.Error as error_carga_db:
            print(" Error en la carga de la base de datos")
            print(error_carga_db)
            exit()


    def run_select(self,sql):
        try:
            self.cursor.execute(sql)
            resultado = self.cursor.fetchall()

        except mysql.connector.Error as error_carga_datos:
            print(" No se puede obtener los datos desde la base de datos ")
            print(error_carga_datos)

        return resultado


    def run_select_filter(self,sql, parametros):
        try:
            self.cursor.execute(sql, parametros)
            resultado = self.cursor.fetchall()

        except mysql.connector.Error as error_carga_datos:
            print(" No se puede obtener los datos desde la base de datos ")
            #print(error_carga_datos)

        return resultado


    def run_sql_consultas(self,sql, parametros):
        try:
            self.cursor.execute(sql, parametros)
            self.db.commit()

        except mysql.connector.Error as error_sql_consulta:
            print(" No se puede realizar consultas ")
            print(error_sql_consulta)
