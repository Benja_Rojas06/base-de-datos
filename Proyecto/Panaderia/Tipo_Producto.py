import tkinter as tk
from tkinter import ttk

class tipo_producto:

    def __init__(self,root, db):

        self.db = db;
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("300x400")
        self.root.title("Tipo Productos")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__congif_buttons_tipo_producto()
        self.__config_treeview_tipo_producto()
        self.root.mainloop()


    def __config_treeview_tipo_producto(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 197, width = 197, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 300)
        self.llenar_treeview_tipo_producto()


    def __congif_buttons_tipo_producto(self):
        tk.Button(self.root, command = self.__agregar_tipo_producto, text="Agregar").place(x = 0, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__modificar_tipo_producto, text="Modificar").place(x = 200, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__eliminar_tipo_producto, text="Eliminar").place(x = 100, y = 350, width = 100, height = 50)


    # Se llena el treeview de datos.
    def llenar_treeview_tipo_producto(self):
        sql = """select * from tipo_producto;"""
        # Ejecuta el select
        data = self.db.run_select ( sql )

        # Si la data es distina a la que hay actualmente...
        if (data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete ( *self.treeview.get_children () )
            for i in data:
                self.treeview.insert ( "", "end", text=(i[0]),
                                       values= (i[1], i[1]), iid =i[0])
            self.data = data  # Actualiza la data


    def __agregar_tipo_producto(self):
        agregar_tipo_producto(self.db, self)


    def __eliminar_tipo_producto(self):
        sql = "select * from tipo_producto where idtipo_producto = %(idtipo_producto)s"
        row_data = self.db.run_select_filter (sql, {"idtipo_producto": self.treeview.focus ()} )[0]
        eliminar_tipo_producto(self.db, self, row_data )


    def __modificar_tipo_producto(self):
        sql = "select * from tipo_producto where idtipo_producto = %(idtipo_producto)s"
        row_data = self.db.run_select_filter ( sql, {"idtipo_producto": self.treeview.focus()} )[0]
        editar_tipo_producto( self.db, self, row_data )


class agregar_tipo_producto:

    def __init__(self,db, padre):
        self.padre = padre
        self.db = db

        self.add = tk.Toplevel()
        self.add.geometry("300x200")
        self.add.title("Agregar Tipo Productos")
        self.add.resizable(width=0, height=0)

        self.__labels()
        self.__entry()
        self.__buttons()


    def __labels(self):
        tk.Label(self.add, text = "Tipo: ").place(x = 0, y = 10, width = 120, height = 20)


    def __entry(self):
        self.entry_tipo_producto = tk.Entry(self.add)
        self.entry_tipo_producto.place(x = 100, y = 10, width = 130, height = 20)


    def __buttons(self):
        tk.Button(self.add, text = "Aceptar", command = self.__insertar).place(x = 75, y = 115, width = 105, height = 25)


    def __insertar(self):
        sql = """insert into tipo_producto (nom_tipo) values(%(nom_tipo)s)"""
        self.db.run_sql_consultas(sql, {"nom_tipo": self.entry_tipo_producto.get()})
        self.add.destroy()
        self.padre.llenar_treeview_tipo_producto()


class editar_tipo_producto:


    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel ()
        self.config_window ()
        self.__config_label ()
        self.__config_entry ()
        self.__config_button ()


    # Configuración de la ventana.
    def config_window(self):
        self.insert_datos.geometry('600x400' )
        self.insert_datos.title ("Editar datos" )
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label (self.insert_datos, text= "Modificar " + (self.row_data[1]) ).place ( x=5, y=10, width=250, height=20 )
        tk.Label (self.insert_datos, text="Nombre: " ).place ( x=0, y=40, width=100, height=20 )


    # Configuración de las casillas que el usuario ingresa info
    def __config_entry(self):
        self.entry_tipo_producto = tk.Entry ( self.insert_datos )
        self.entry_tipo_producto.place ( x=100, y=40, width=130, height=20)
        self.entry_tipo_producto.insert ( 0, self.row_data[0])


    # Configuración de los botones
    def __config_button(self):
        tk.Button ( self.insert_datos, text="Aceptar",
                    command=self.modificar ).place ( x=75, y=200, width=105, height=25 )


    # Insercion en la base de datos.
    def modificar(self):
        sql = """update tipo_producto set nom_tipo = %(nom_tipo)s where idtipo_producto = %(idtipo_producto)s"""

        self.db.run_sql_consultas(sql,{"nom_tipo" : self.entry_tipo_producto.get(),
                                 "idtipo_producto": int(self.row_data[0])})

        self.insert_datos.destroy ()
        self.padre.llenar_treeview_tipo_producto()


class eliminar_tipo_producto:


    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.delete_datos = tk.Toplevel ()
        self.config_window ()
        self.__config_label ()
        self.__config_button ()


    # Configuración de la ventana.
    def config_window(self):
        self.delete_datos.geometry ('300x130')
        self.delete_datos.title ("Eliminar Tipos de Productos")
        self.delete_datos.resizable (width=0, height=0)


    # Configuración de los labels.
    def __config_label(self):
        tk.Label (self.delete_datos, text= "Se eliminarán todos los productos de tipo: ").place (x=25, y=10, width=250, height=20)
        tk.Label (self.delete_datos, text= "-> " + (self.row_data[1])).place(x=25, y=30, width=250, height=20)
        tk.Label (self.delete_datos, text= "¿Está seguro de que desea eliminar?").place(x=25, y=50, width=250, height=20)


    # Configuración de los botones.
    def __config_button(self):
        ttk.Button(self.delete_datos, command = self.aceptar, text="Si").place(x = 25, y = 80, width = 100, height = 50)
        ttk.Button(self.delete_datos, command = self.cancelar, text="No").place(x = 175, y = 80, width = 100, height = 50)


    # Se lleva a cabo la eliminación.
    def aceptar(self):
        sql = "delete from tipo_producto where idtipo_producto = %(idtipo_producto)s"
        self.db.run_sql_consultas(sql, {"idtipo_producto": int(self.row_data[0])})
        self.delete_datos.destroy ()
        self.padre.llenar_treeview_tipo_producto()


    # Se cierra la ventana de eliminación.
    def cancelar(self):
        self.delete_datos.destroy()


