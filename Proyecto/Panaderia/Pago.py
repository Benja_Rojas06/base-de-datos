import tkinter as tk
from tkinter import ttk

class pago:

    def __init__(self,root, db):

        self.db = db;
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("300x400")
        self.root.title("Tipo de Pago")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__config_buttons_pago()
        self.__config_treview_pago()
        self.root.mainloop()


    def __config_treview_pago(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 197, width = 197, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 300)
        self.llenar_treeview_pago()

    def __config_buttons_pago(self):
        tk.Button(self.root, command = self.__agregar_pago, text="Agregar").place(x = 0, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__modificar_pago, text="Modificar").place(x = 200, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__eliminar_pago, text="Eliminar").place(x = 100, y = 350, width = 100, height = 50)


    def llenar_treeview_pago(self):  # Se llena el treeview de datos.
        sql = """select * from metodo_pago;"""
        # Ejecuta el select
        data = self.db.run_select ( sql )

        # Si la data es distina a la que hay actualmente...
        if (data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete ( *self.treeview.get_children () )
            for i in data:
                self.treeview.insert ( "", "end", text=(i[0]),
                                       values= (i[1], i[1]), iid =i[0])
            self.data = data  # Actualiza la data

    def __agregar_pago(self):
        agregar_pago(self.db, self)

    def __eliminar_pago(self):
        sql = "delete from metodo_pago where id_met = %(id_met)s"
        self.db.run_sql_consultas( sql, {"id_met": self.treeview.focus ()})
        self.llenar_treeview_pago()

    def __modificar_pago(self):
        sql = "select * from metodo_pago where id_met = %(id_met)s"
        row_data = self.db.run_select_filter ( sql, {"id_met": self.treeview.focus()} )[0]
        editar_pago( self.db, self, row_data )

class agregar_pago:

    def __init__(self,db, padre):
        self.padre = padre
        self.db = db

        self.add = tk.Toplevel()
        self.add.geometry("300x200")
        self.add.title("Agregar Tipo de Pago")
        self.add.resizable(width=0, height=0)

        self.__labels()
        self.__entry()
        self.__buttons()


    def __labels(self):
        tk.Label(self.add, text = "Tipo: ").place(x = 0, y = 10, width = 120, height = 20)


    def __entry(self):
        self.entry_pago = tk.Entry(self.add)
        self.entry_pago.place(x = 100, y = 10, width = 130, height = 20)

    def __buttons(self):
        tk.Button(self.add, text = "Aceptar", command = self.__insertar).place(x = 75, y = 115, width = 105, height = 25)

    def __insertar(self):
        sql = """insert into metodo_pago (nom_met) values(%(nom_met)s)"""
        self.db.run_sql_consultas(sql, {"nom_met": self.entry_pago.get()})
        self.add.destroy()
        self.padre.llenar_treeview_pago()


class editar_pago:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel ()
        self.config_window ()
        self.__config_label ()
        self.__config_entry ()
        self.__config_button ()

    def config_window(self):  # Configuración de la ventana.
        self.insert_datos.geometry('600x400' )
        self.insert_datos.title ("Editar datos" )
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label ( self.insert_datos, text= "Modificar " + (self.row_data[1]) ).place ( x=5, y=10, width=250, height=20 )
        tk.Label ( self.insert_datos, text="Nombre: " ).place ( x=0, y=40, width=100, height=20 )


        # Configuración de las casillas que el usuario ingresa info

    def __config_entry(self):
        self.entry_pago = tk.Entry ( self.insert_datos )
        self.entry_pago.place ( x=100, y=40, width=130, height=20)
        self.entry_pago.insert ( 0, self.row_data[0])

        # Configuración de los botones

    def __config_button(self):
        tk.Button ( self.insert_datos, text="Aceptar",
                    command=self.modificar ).place ( x=75, y=200, width=105, height=25 )

    def modificar(self):  # Insercion en la base de datos.
        sql = """update metodo_pago set nom_met = %(nom_met)s where id_met = %(id_met)s"""

        self.db.run_sql_consultas(sql,{"nom_met" : self.entry_pago.get(),
                                 "id_met": int(self.row_data[0])})

        self.insert_datos.destroy ()
        self.padre.llenar_treeview_pago()
