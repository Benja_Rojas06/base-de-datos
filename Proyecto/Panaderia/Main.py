from DB import Base_de_datos
from App import App

def main():
    # Conecta a la base de datos
    db = Base_de_datos()

    # Ejecuta la app de Tía Carmela
    App(db)

if __name__ == "__main__":
    main()