import tkinter as tk
from tkinter import Menu
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from PIL import Image, ImageTk
from DB import Base_de_datos
from Cliente import cliente
from Pago import pago
from Poblacion import poblacion
from Tipo_Producto import tipo_producto
from Sector import sector
from Poblacion import poblacion
from Producto import producto

#
class App:
    def __init__(self, db):
        self.db = db

        # Main window
        self.root = tk.Tk()

        # Algunas especificaciones de tamaño y título de la ventana
        self.root.geometry("700x400")
        self.root.title("Panadería Tía Carmela")

        #
        self.__crea_menubar()
        self.__crea_botones_principales()
        self.__agrega_imagen_principal()

        # Empieza a correr la interfaz.
        self.root.mainloop()

    # menubar
    def __crea_menubar(self):
        menubar = Menu(self.root)
        self.root.config(menu=menubar)

        #
        file_menu = Menu(menubar, tearoff=False)
        file_menu.add_command (label='Salir',
                                command=self.root.destroy)

        #
        menubar.add_cascade(label="Información", menu=file_menu)
        # Desplegar ventana con info de como hacer ingreso de información.


    # botones principales.
    def __crea_botones_principales(self):
        padx = 2
        pady = 2

        frame = LabelFrame(self.root, text="", relief=tk.GROOVE)
        frame.place(x=10, y=10, width=160, relheight=0.95)

        b1 = Button(frame, text="Clientes", width=20)
        b1.grid(row=0, column=0, padx=padx, pady=pady)
        b1.bind('<Button-1>', self.__mostrar_clientes)

        b2 = Button(frame, text="Productos", width=20)
        b2.grid(row=1, column=0, padx=padx, pady=pady)
        b2.bind('<Button-1>', self.__mostrar_producto)

        b3 = Button(frame, text = "Tipo Producto", width=20)
        b3.grid(row=2, column=0, padx=padx, pady = pady)
        b3.bind('<Button-1>',self.__mostrar_tipo_producto)

        b4 = Button(frame, text = "Venta", width=20)
        b4.grid(row=3, column=0, padx=padx, pady = pady)
        b4.bind('<Button-1>')

        b5 = Button(frame, text = "Registro de Venta", width=20)
        b5.grid(row=4, column=0, padx=padx, pady = pady)
        b5.bind('<Button-1>')

        b6 = Button(frame, text="Tipo de Pago", width=20)
        b6.grid(row=5, column=0, padx=padx, pady=pady)
        b6.bind('<Button-1>', self.__mostrar_pago)

        b7 = Button(frame, text = "Dirección", width=20)
        b7.grid(row=6, column=0, padx=padx, pady = pady)
        b7.bind('<Button-1>')

        b8 = Button(frame, text = "Población", width=20)
        b8.grid(row=7, column=0, padx=padx, pady = pady)
        b8.bind('<Button-1>', self.__mostrar_poblacion)

        b9 = Button(frame, text = "Sector", width=20)
        b9.grid(row=8, column=0, padx=padx, pady = pady)
        b9.bind('<Button-1>', self.__mostrar_sector)


    # imagen principal.
    def __agrega_imagen_principal(self):
        #
        frame = LabelFrame(self.root, text="", relief=tk.FLAT)
        frame.place(x=215, y=10, relwidth=0.68, relheight=0.95)

        image = Image.open("a.jpeg")
        photo = ImageTk.PhotoImage(image.resize((450, 450), Image.ANTIALIAS))
        label = Label(frame, image=photo)
        label.image = photo
        label.pack()


    # muestra ventana jugadores.

    def __mostrar_clientes(self, button):
        cliente(self.root, self.db)

    def __mostrar_producto(self, button):
        producto(self.root, self.db)

    def __mostrar_tipo_producto(self,button):
        tipo_producto(self.root, self.db)

    def __mostrar_pago(self,button):
        pago(self.root,self.db)

    def __mostrar_sector(self,button):
        sector(self.root,self.db)

    def __mostrar_poblacion(self,button):
        poblacion(self.root,self.db)


