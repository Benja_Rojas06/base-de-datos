import tkinter as tk
from tkinter import ttk
from tkinter.constants import CENTER


class cliente:

    def __init__(self,root, db):

        self.db = db;
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("885x600")
        self.root.title("Cliente")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__config_buttons_cliente()
        self.__config_treview_cliente()
        self.root.mainloop()


    def __config_treview_cliente(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns=("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Rut")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        self.treeview.heading("#3", text = "Correo")
        self.treeview.heading("#4", text = "Teléfono")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#3", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#4", minwidth = 148, width = 148, stretch = False)
        self.treeview.place(x = 20, y = 0, height = 500, width = 850)
        self.llenar_treeview_cliente()
        self.root.after ( 0, self.llenar_treeview_cliente )

    def __config_buttons_cliente(self):
        tk.Button(self.root, command = self.__agregar_cliente, text="Agregar Cliente").place(x = 100, y = 520, width = 200, height = 50)
        tk.Button(self.root, command = self.__modificar_cliente, text="Modificar Cliente").place(x = 350, y = 520, width = 200, height = 50)
        tk.Button(self.root, command = self.__eliminar_cliente, text="Eliminar Cliente").place(x = 600, y = 520, width = 200, height = 50)


    def llenar_treeview_cliente(self):  # Se llena el treeview de datos.
        sql = """select * from cliente;"""
        # Ejecuta el select
        data = self.db.run_select ( sql )

        # Si la data es distina a la que hay actualmente...
        if (data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete ( *self.treeview.get_children () )
            for i in data:
                self.treeview.insert ( "", "end", text=(i[0]),
                                       values= (i[1], i[2], i[3], i[4]), iid =i[0])
            self.data = data  # Actualiza la data

    def __agregar_cliente(self):
        agregar_cliente(self.db, self)

    def __eliminar_cliente(self):
        sql = "delete from cliente where rut_cliente = %(rut_cliente)s"
        self.db.run_sql_consultas( sql, {"rut_cliente": self.treeview.focus ()})
        self.llenar_treeview_cliente ()

    def __modificar_cliente(self):
        sql = "select * from cliente where rut_cliente = %(rut_cliente)s"
        row_data = self.db.run_select_filter ( sql, {"rut_cliente": self.treeview.focus ()} )[0]
        editar_cliente (self.db, self, row_data )

class agregar_cliente:

    def __init__(self,db, padre):
        self.padre = padre
        self.db = db

        self.add = tk.Toplevel()
        self.add.geometry("300x200")
        self.add.title("Agregar Clientes")
        self.add.resizable(width=0, height=0)

        self.__labels()
        self.__entry()
        self.__buttons()


    def __labels(self):
        tk.Label(self.add, text = "Rut: ").place(x = 0, y = 10, width = 120, height = 20)
        tk.Label(self.add, text = "Nombre: ").place(x = 0, y = 30, width = 120, height = 20)
        tk.Label(self.add, text = "Apellido: ").place(x = 0, y = 50, width = 120, height = 20)
        tk.Label(self.add, text = "Correo: ").place(x = 0, y = 70, width = 120, height = 20)
        tk.Label(self.add, text = "Teléfono: ").place(x = 0, y = 90, width = 120, height = 20)

    def __entry(self):
        self.entry_rut = tk.Entry(self.add)
        self.entry_rut.place(x = 100, y = 10, width = 130, height = 20)
        self.entry_nombre = tk.Entry(self.add)
        self.entry_nombre.place(x = 100, y = 30, width = 130, height = 20)
        self.entry_apellido = tk.Entry(self.add)
        self.entry_apellido.place(x = 100, y = 50, width = 130, height = 20)
        self.entry_correo = tk.Entry(self.add)
        self.entry_correo.place(x = 100, y = 70, width = 130, height =20)
        self.entry_telefono = tk.Entry(self.add)
        self.entry_telefono.place(x = 100, y = 90, width = 130, height =20)

    def __buttons(self):
        tk.Button(self.add, text = "Aceptar", command = self.__insertar).place(x = 75, y = 115, width = 105, height = 25)

    def __insertar(self):
        sql = """insert into cliente (rut_cliente, nom_cliente, ape_cliente, correo, telefono) values(%(rut_cliente)s, %(nom_cliente)s,
                                      %(ape_cliente)s, %(correo)s, %(telefono)s)"""
        self.db.run_sql_consultas(sql, {"rut_cliente": self.entry_rut.get(), "nom_cliente": self.entry_nombre.get(),
                                        "ape_cliente": self.entry_apellido.get(), "correo": self.entry_correo.get(),
                                        "telefono": self.entry_telefono.get()})
        self.add.destroy()
        self.padre.llenar_treeview_cliente()


class editar_cliente:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel ()
        self.config_window ()
        self.__config_label ()
        self.__config_entry ()
        self.__config_button ()

    def config_window(self):  # Configuración de la ventana.
        self.insert_datos.geometry('400x300' )
        self.insert_datos.title ("Editar datos" )
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label ( self.insert_datos, text="Rut: " ).place ( x=0, y=40, width=100, height=20 )
        tk.Label ( self.insert_datos, text="Nombre: " ).place ( x=0, y=70, width=100, height=20 )
        tk.Label ( self.insert_datos, text="Apellido: " ).place ( x=0, y=100, width=100, height=20 )
        tk.Label ( self.insert_datos, text="Correo: " ).place ( x=0, y=130, width=100, height=20 )
        tk.Label ( self.insert_datos, text="Teléfono: " ).place ( x=0, y=160, width=100, height=20 )

        # Configuración de las casillas que el usuario ingresa info

    def __config_entry(self):
        self.entry_rut = tk.Entry ( self.insert_datos )
        self.entry_rut.place ( x=100, y=40, width=130, height=20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x=100,y=70, width=130, height=20)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x=100, y=100, width=130, height=20)
        self.entry_correo = tk.Entry(self.insert_datos)
        self.entry_correo.place(x=100,y=130, width=130, height=20)
        self.entry_telefono = tk.Entry(self.insert_datos)
        self.entry_telefono.place(x=100,y=160, width=130, height=20)
        self.entry_rut.insert ( 0, self.row_data[0])
        self.entry_nombre.insert(0,self.row_data[1])
        self.entry_apellido.insert(0,self.row_data[2])
        self.entry_correo.insert(0,self.row_data[3])
        self.entry_telefono.insert(0,self.row_data[4])

        # Configuración de los botones

    def __config_button(self):
        tk.Button ( self.insert_datos, text="Aceptar",
                    command=self.modificar ).place ( x=75, y=200, width=105, height=25 )

    def modificar(self):  # Insercion en la base de datos.
        sql = """update cliente set rut_cliente = %(rut_cliente)s, nom_cliente = %(nom_cliente)s,
                ape_cliente = %(ape_cliente)s, correo = %(correo)s, telefono = %(telefono)s"""

        self.db.run_sql_consultas(sql,{"rut_cliente": self.entry_rut.get(), "nom_cliente": self.entry_nombre.get(),
                                 "ape_cliente": self.entry_apellido.get(), "correo": self.entry_correo.get(),
                                 "telefono": self.entry_telefono.get()})

        self.insert_datos.destroy ()
        self.padre.llenar_treeview_cliente()
