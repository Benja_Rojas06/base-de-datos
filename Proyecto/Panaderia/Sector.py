import tkinter as tk
from tkinter import ttk

class sector:

    def __init__(self,root, db):

        self.db = db;
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("300x400")
        self.root.title("Sector")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__config_buttons_sector()
        self.__config_treview_sector()
        self.root.mainloop()


    def __config_treview_sector(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 197, width = 197, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 300)
        self.llenar_treview_sector()

    def __config_buttons_sector(self):
        tk.Button(self.root, command = self.__agregar_sector, text="Agregar").place(x = 0, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__modificar_sector, text="Modificar").place(x = 200, y = 350, width = 100, height = 50)
        tk.Button(self.root, command = self.__eliminar_sector, text="Eliminar").place(x = 100, y = 350, width = 100, height = 50)


    def llenar_treview_sector(self):  # Se llena el treeview de datos.
        sql = """select * from sector;"""
        # Ejecuta el select
        data = self.db.run_select ( sql )

        # Si la data es distina a la que hay actualmente...
        if (data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete ( *self.treeview.get_children () )
            for i in data:
                self.treeview.insert ( "", "end", text=(i[0]),
                                       values= (i[1], i[1]), iid =i[0])
            self.data = data  # Actualiza la data

    def __agregar_sector(self):
        agregar_sector(self.db, self)

    def __eliminar_sector(self):
        sql = "delete from sector where id_sector = %(id_sector)s"
        self.db.run_sql_consultas( sql, {"id_sector": self.treeview.focus ()})
        self.llenar_treview_sector()

    def __modificar_sector(self):
        sql = "select * from sector where id_sector = %(id_sector)s"
        row_data = self.db.run_select_filter ( sql, {"id_sector": self.treeview.focus()} )[0]
        editar_sector( self.db, self, row_data )

class agregar_sector:

    def __init__(self,db, padre):
        self.padre = padre
        self.db = db

        self.add = tk.Toplevel()
        self.add.geometry("300x200")
        self.add.title("Agregar Sector")
        self.add.resizable(width=0, height=0)

        self.__labels()
        self.__entry()
        self.__buttons()


    def __labels(self):
        tk.Label(self.add, text = "Sector: ").place(x = 0, y = 10, width = 120, height = 20)


    def __entry(self):
        self.entry_sector = tk.Entry(self.add)
        self.entry_sector.place(x = 100, y = 10, width = 130, height = 20)

    def __buttons(self):
        tk.Button(self.add, text = "Aceptar", command = self.__insertar).place(x = 75, y = 115, width = 105, height = 25)

    def __insertar(self):
        sql = """insert into sector (nom_sector) values(%(nom_sector)s)"""
        self.db.run_sql_consultas(sql, {"nom_sector": self.entry_sector.get()})
        self.add.destroy()
        self.padre.llenar_treview_sector()


class editar_sector:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel ()
        self.config_window ()
        self.__config_label ()
        self.__config_entry ()
        self.__config_button ()

    def config_window(self):  # Configuración de la ventana.
        self.insert_datos.geometry('600x400' )
        self.insert_datos.title ("Editar datos" )
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label ( self.insert_datos, text= "Modificar " + (self.row_data[1]) ).place ( x=5, y=10, width=250, height=20 )
        tk.Label ( self.insert_datos, text="Nombre: " ).place ( x=0, y=40, width=100, height=20 )


        # Configuración de las casillas que el usuario ingresa info

    def __config_entry(self):
        self.entry_sector = tk.Entry ( self.insert_datos )
        self.entry_sector.place ( x=100, y=40, width=130, height=20)
        self.entry_sector.insert ( 0, self.row_data[0])

        # Configuración de los botones

    def __config_button(self):
        tk.Button ( self.insert_datos, text="Aceptar",
                    command=self.modificar ).place ( x=75, y=200, width=105, height=25 )

    def modificar(self):  # Insercion en la base de datos.
        sql = """update sector set nom_sector = %(nom_sector)s where id_sector = %(id_sector)s"""

        self.db.run_sql_consultas(sql,{"nom_sector" : self.entry_sector.get(),
                                 "id_sector": int(self.row_data[0])})

        self.insert_datos.destroy ()
        self.padre.llenar_treview_sector()
