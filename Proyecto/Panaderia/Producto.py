import tkinter as tk
from tkinter import ttk
from Tipo_Producto import tipo_producto

class producto:

    def __init__(self,root, db):

        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry("700x400")
        self.root.title("Producto")
        self.root.resizable(width=0, height=0)
        self.root.transient(root)

        self.__config_treeview_producto()
        self.__config_buttons_producto()


    def __config_treeview_producto(self):

        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns =("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Producto")
        self.treeview.heading("#2", text = "Precio")
        self.treeview.heading("#3", text = "Tipo Producto")

        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#3", minwidth = 197, width = 197, stretch = False)
        self.treeview.place(x =0, y = 0, height = 350, width = 700)
        self.llenar_treeview_producto()
        self.root.after(0,self.llenar_treeview_producto)

    def __config_buttons_producto(self):
        tk.Button(self.root, text = "Insertar Producto",
                command = self.__insertar_producto).place(x = 50, y = 350, width =200, height =50)
        tk.Button(self.root, text="Modificar Producto",
                command = self.__modificar_producto).place(x = 250, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar Producto",
                command = self.__eliminar_producto).place(x = 450, y = 350, width = 200, height = 50)

    def llenar_treeview_producto(self):

        sql = """select producto.id_producto, producto.nom_producto, producto.precio, tipo_producto.nom_tipo from producto inner join tipo_producto on producto.tipo_producto_idtipo_producto = tipo_producto.idtipo_producto"""

        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                                    values = (i[1], i[2], i[3]),
                                    iid = i[0])

            self.data = data


    def __insertar_producto(self):
        insertar_producto(self.db, self)


    def __eliminar_producto(self):

        sql = "delete from producto where id_producto = %(id_producto)s"

        self.db.run_sql_consultas(sql, {"id_producto": self.treeview.focus()})
        self.llenar_treeview_producto()


    def __modificar_producto(self):
        if(self.treeview.focus() != ""):
            sql = """select producto.id_producto, producto.nom_producto, producto.precio, tipo_producto.nom_tipo
            from producto inner join tipo_producto on producto.tipo_producto_idtipo_producto = tipo_producto.idtipo_producto
            where id_producto = %(id_producto)s;"""
            row_data = self.db.run_select_filter(sql, {"id_producto": self.treeview.focus()})[0]
            modificar_producto(self.db, self, row_data)


class insertar_producto:

    def __init__(self,db,padre):

        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel ()
        self.__config_entry()
        self.__config_label()
        self.__config_entry()
        self.__config_button()
        self.__config_window()


    def __config_window(self):
        self.insert_datos.geometry("200x120")
        self.insert_datos.title("Insertar Producto")
        self.insert_datos.resizable(width=0, height=0)


    def __config_label(self):
        tk.Label(self.insert_datos, text = "Producto: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Precio: ").place(x = 10, y = 40, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Tipo Producto: ").place(x = 10, y = 70, width = 80, height = 20)

    def __config_entry(self):

        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 110, y = 40, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.__fill_combo()


    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)


    def __fill_combo(self):
        sql = "select idtipo_producto, nom_tipo from tipo_producto"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): # Inserción en la base de datos.
        sql = """insert  into producto(tipo_producto_idtipo_producto, nom_producto, precio)
            values(%(tipo_producto_idtipo_producto)s, %(nom_producto)s, %(precio)s)"""
        self.db.run_sql_consultas(sql, {"tipo_producto_idtipo_producto": self.ids[self.combo.current()],
            "nom_producto": self.entry_nombre.get(), "precio": int(self.entry_precio.get())})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_producto()


class modificar_producto:


    def __init__(self, db, padre, row_data):

        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()


    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar Producto")
        self.insert_datos.resizable(width=0, height=0)


    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Producto: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Precio: ").place(x = 10, y = 40, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Tipo Producto: ").place(x = 10, y = 70, width = 80, height = 20)


    def config_entry(self):#Se configuran los inputs

        self.entry_producto = tk.Entry(self.insert_datos)
        self.entry_producto.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 110, y = 30, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.fill_combo()
        self.entry_producto.insert(0, self.row_data[1])
        self.entry_producto.insert(0, self.row_data[2])
        self.combo.insert(0, self.row_data[1])


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)


    def modificar(self): #Inserción en la base de datos.
        sql = """update producto set nom_producto = %(nom_producto)s, precio = %(precio)s,
            tipo_producto_idtipo_producto = %(tipo_producto_idtipo_producto)s where id_producto = %(id_producto)s"""
        self.db.run_sql_consultas(sql, {"nom_producto": self.entry_producto.get(),
                                        "precio": int(self.entry_precio.get()),
                                        "tipo_producto_idtipo_producto": self.ids[self.combo.current()]
                                        })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_producto()


    def fill_combo(self):
        sql = "select idtipo_producto, nom_tipo from tipo_producto"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
